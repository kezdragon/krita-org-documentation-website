# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 18:28+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:0
msgid ".. image:: images/layers/fill_layer_simplex_noise.png"
msgstr ".. image:: images/layers/fill_layer_simplex_noise.png"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:1
msgid "How to use fill layers in Krita."
msgstr "Hur man använder fyllager i Krita."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Layers"
msgstr "Lager"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Fill"
msgstr "Fyll"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Generator"
msgstr "Generering"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:17
msgid "Fill Layers"
msgstr "Fyllager"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:19
msgid ""
"A Fill Layer is a special layer that Krita generates on-the-fly that can "
"contain either a pattern or a solid color."
msgstr ""
"Ett fyllager är ett speciellt lager som Krita genererar i farten som "
"antingen kan innehålla ett mönster eller en hel färg."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:22
msgid ".. image:: images/layers/Fill_Layer.png"
msgstr ".. image:: images/layers/Fill_Layer.png"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:24
msgid ""
"This fills the layer with a predefined pattern or texture that has been "
"loaded into Krita through the Resource Management interface.  Patterns can "
"be a simple and interesting way to add texture to your drawing or painting, "
"helping to recreate the look of watercolor paper, linen, canvas, hardboard, "
"stone or an infinite other number of options.  For example if you want to "
"take a digital painting and finish it off with the appearance of it being on "
"canvas you can add a Fill Layer with the Canvas texture from the texture "
"pack below and set the opacity very low so the \"threads\" of the pattern "
"are just barley visible.  The effect is quite convincing."
msgstr ""
"Fyller lagret med ett fördefinierat mönster eller struktur som har lästs in "
"av Krita via resurshanteringsgränssnittet. Mönster kan vara ett enkelt och "
"intressant sätt att lägga till struktur i en teckning eller målning, och "
"hjälper till att skapa utseendet hos vattenfärgspapper, linne, målarduk, "
"träfiberskivor, sten eller ett oändligt antal andra alternativ. Om man "
"exempelvis vill ta en digital målning och avsluta den så att den ser ut som "
"om den är skapad på målarduk, kan man lägga till ett fyllager med strukturen "
"Duk från strukturpacken nedan och ställa in mycket låg ogenomskinlighet så "
"att \"trådarna\" i mönstret precis är synliga. Effekten är mycket "
"övertygande."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:26
msgid "Pattern"
msgstr "Mönster"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:26
msgid ""
"You can create your own and use those as well.  For a great set of well "
"designed and useful patterns check out one of our favorite artists and a "
"great friend of Krita, David Revoy's free texture pack (https://www."
"davidrevoy.com/article156/texture-pack-1)."
msgstr ""
"Man kan också skapa sina egna och använda dem. För en utmärkt uppsättning "
"välkonstruerade och användbara mönster, ta en titt på den fria "
"strukturpacken (https://www.davidrevoy.com/article156/texture-pack-1) av "
"David Revoy, en av våra favoritkonstnärer och en stor vän av Krita."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:29
msgid "Color"
msgstr "Färg"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:29
msgid ""
"The second option is not quite as exciting, but does the job. Fill the layer "
"with a selected color."
msgstr ""
"Det andra alternativet är inte riktigt så spännande, men gör vad det ska. "
"Fyll lagret med den valda färgen."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:36
msgid ""
"A noise generator that isn't Perline Noise (which is what typical 'clouds' "
"generation is), but it looks similar and can actually loop. Uses the "
"OpenSimplex code."
msgstr ""
"En brusgenerator som inte skapar Perlin-brus (vilket är vad generering av "
"'moln' typisk använder), men ser liknande ut och faktiskt kan upprepas. "
"Använder OpenSimplex-koden."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:38
msgid "Looping"
msgstr "Upprepning"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:39
msgid "Whether or not to force the pattern to loop."
msgstr "Om mönstret ska tvingas att upprepas."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:40
msgid "Frequency"
msgstr "Frekvens"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:41
msgid ""
"The frequency of the waves used to generate the pattern. Higher frequency "
"results in a finer noise pattern."
msgstr ""
"Frekvens hos vågorna som används för att skapa mönstret. Högre frekvens "
"resulterar i ett finare brusmönster."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:42
msgid "Ratio"
msgstr "Förhållande"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:43
msgid ""
"The ratio of the waves in the x and y dimensions. This makes the noise have "
"a rectangular appearance."
msgstr ""
"Vågornas förhållande i x- och y-riktningarna. Det gör att bruset får ett "
"rektangulärt utseende."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid "Simplex Noise"
msgstr "Simplexbrus"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid "Use Custom Seed"
msgstr "Använd eget frö"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid ""
"The seed for the random component. You can input any value or text here, and "
"it will always try to use this value to generate the random values with "
"(which then are always the same for a given seed)."
msgstr ""
"Fröet för slumpkomponenten. Man kan skriva in vilket värde eller text som "
"helst här, och det försöker alltid använda värdet för att generera "
"slumpvärdena (vilka då alltid är samma för ett givet frö)."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:48
msgid "Painting on a fill layer"
msgstr "Måla på ett fyllager"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:50
msgid ""
"A fill-layer is a single-channel layer, meaning it only has transparency. "
"Therefore, you can erase and paint on fill-layers to make them semi-opaque, "
"or for when you want to have a particular color only. Being single channel, "
"fill-layers are also a little bit less memory-consuming than regular 4-"
"channel paint layers."
msgstr ""
"Ett fyllager är ett enkanalslager, vilket betyder att det bara har "
"genomskinlighet. Därför kan man radera och måla på fyllager för att göra dem "
"halvgenomskinliga, eller i fallet då man bara vill ha en viss färg. Genom "
"att de är enkanaliga använder fyllager också lite mindre minne en vanliga "
"fyrkanalers målarlager."
