# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:34+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_gih.rst:1
msgid "The Gimp Image Hose file format in Krita."
msgstr "GIMP-bildslangfilformat i Krita."

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "Image Hose"
msgstr "Bildslang"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "Gimp Image Hose"
msgstr "GIMP-bildslang"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "GIH"
msgstr "GIH"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "*.gih"
msgstr "*.gih"

#: ../../general_concepts/file_formats/file_gih.rst:15
msgid "\\*.gih"
msgstr "\\*.gih"

#: ../../general_concepts/file_formats/file_gih.rst:17
msgid ""
"The GIMP image hose format. Krita can open and save these, as well as import "
"via the :ref:`predefined brush tab <predefined_brush_tip>`."
msgstr ""
"GIMP-bildslangformatet. Krita kan öppna och spara det, samt importera via :"
"ref:`fliken fördefinerade penslar tab <predefined_brush_tip>`."

#: ../../general_concepts/file_formats/file_gih.rst:19
msgid ""
"Image Hose means that this file format allows you to store multiple images "
"and then set some options to make it specify how to output the multiple "
"images."
msgstr ""
"Bildslang betyder att filformatet låter dig lagra flera bilder och sedan "
"ställa in några alternativ för att specificera hur alla bilderna ska matas "
"ut."

#: ../../general_concepts/file_formats/file_gih.rst:25
msgid ".. image:: images/brushes/Gih-examples.png"
msgstr ".. image:: images/brushes/Gih-examples.png"

#: ../../general_concepts/file_formats/file_gih.rst:25
msgid "From top to bottom: Incremental, Pressure and Random"
msgstr "Från topp till botten: Inkrementell, Tryck och Slumpmässig"

#: ../../general_concepts/file_formats/file_gih.rst:27
msgid "Gimp image hose format options:"
msgstr "Alternativ för GIMP-bildslangfilformatet:"

#: ../../general_concepts/file_formats/file_gih.rst:29
msgid "Constant"
msgstr "Konstant"

#: ../../general_concepts/file_formats/file_gih.rst:30
msgid "This'll use the first image, no matter what."
msgstr "Använder första bilden oavsett."

#: ../../general_concepts/file_formats/file_gih.rst:31
msgid "Incremental"
msgstr "Inkrementell"

#: ../../general_concepts/file_formats/file_gih.rst:32
msgid ""
"This'll paint the image layers in sequence. This is good for images that can "
"be strung together to create a pattern."
msgstr ""
"Målar bildlagren i sekvens. Det är bra för bilder som kan knytas samman för "
"att skapa ett mönster."

#: ../../general_concepts/file_formats/file_gih.rst:33
msgid "Pressure"
msgstr "Tryck"

#: ../../general_concepts/file_formats/file_gih.rst:34
msgid ""
"This'll paint the images depending on pressure. This is good for brushes "
"imitating the hairs of a natural brush."
msgstr ""
"Målar bilderna beroende på tryck. Det är bra för penslar som imiterar håren "
"i en naturlig pensel."

#: ../../general_concepts/file_formats/file_gih.rst:35
msgid "Random"
msgstr "Slumpmässig"

#: ../../general_concepts/file_formats/file_gih.rst:36
msgid ""
"This'll draw the images randomly. This is good for image-collections used in "
"speedpainting as well as images that generate texture. Or perhaps more "
"graphical symbols."
msgstr ""
"Ritar bilderna slumpmässigt. Det är bra för bildsamlingar använda vid "
"snabbmålning samt bilder som skapar en struktur. Eller kanske mer grafiska "
"symboler."

#: ../../general_concepts/file_formats/file_gih.rst:38
msgid "Angle"
msgstr "Vinkel"

#: ../../general_concepts/file_formats/file_gih.rst:38
msgid "This'll use the dragging angle to determine with image to draw."
msgstr "Det använder dragvinkeln för att bestämma vilken bild som ska ritas."

#: ../../general_concepts/file_formats/file_gih.rst:40
msgid ""
"When exporting a Krita file as a ``.gih``, you will also get the option to "
"set the default spacing, the option to set the name (very important for "
"looking it up in the UI) and the ability to choose whether or not to "
"generate the mask from the colors."
msgstr ""
"Vid export av en Krita-fil som en ``.gih``, får man också alternativet att "
"ange standardmellanrummet, alternativet för att ange namnet (mycket viktigt "
"för att slå upp den i användargränssnittet), och möjlighet att välja om "
"masken ska genereras från färgerna eller inte."

#: ../../general_concepts/file_formats/file_gih.rst:43
msgid "Use Color as Mask"
msgstr "Använd färg som mask"

#: ../../general_concepts/file_formats/file_gih.rst:43
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""
"Det ändrar de mörkaste värdena på bilden till de som målar, och de ljusaste "
"till genomskinliga. Avmarkera det om färglagda bilder används för penseln.."

#: ../../general_concepts/file_formats/file_gih.rst:45
msgid ""
"We have a :ref:`Krita Brush tip page <brush_tip_animated_brush>` on how to "
"create your own gih brush."
msgstr ""
"Det finns en :ref:`Krita penselspetssida <brush_tip_animated_brush>` om hur "
"man skapar sin egen gih-pensel."
