msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___dockers___snap_settings_docker.pot\n"

#: ../../<generated>:1
msgid "Guides"
msgstr "参考线"

#: ../../reference_manual/dockers/snap_settings_docker.rst:1
msgid "Overview of the snap settings docker."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:11
msgid "Snap"
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:16
msgid "Snap Settings"
msgstr "吸附选项"

#: ../../reference_manual/dockers/snap_settings_docker.rst:20
msgid ""
"This docker has been removed in Krita 3.0. For more information on how to do "
"this instead, consult the :ref:`snapping page <snapping>`."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:23
msgid ".. image:: images/dockers/Krita_Snap_Settings_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:24
msgid ""
"This is docker only applies for Vector Layers. Snapping determines where a "
"vector shape will snap. The little number box is for snapping to a grid."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:26
msgid "Node"
msgstr "结点"

#: ../../reference_manual/dockers/snap_settings_docker.rst:27
msgid "For snapping to other vector nodes."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:28
msgid "Extensions of Line"
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:29
msgid ""
"For snapping to a point that could have been part of a line, had it been "
"extended."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:30
msgid "Bounding Box"
msgstr "边框"

#: ../../reference_manual/dockers/snap_settings_docker.rst:31
msgid "For snapping to the bounding box of a vector shape."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:32
msgid "Orthogonal"
msgstr "正交"

#: ../../reference_manual/dockers/snap_settings_docker.rst:33
msgid "For snapping to only horizontal or vertical lines."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:34
msgid "Intersection"
msgstr "交界"

#: ../../reference_manual/dockers/snap_settings_docker.rst:35
msgid "For snapping to other vector lines."
msgstr ""

#: ../../reference_manual/dockers/snap_settings_docker.rst:37
msgid "Guides don't exist in Krita, therefore this one is useless."
msgstr ""
