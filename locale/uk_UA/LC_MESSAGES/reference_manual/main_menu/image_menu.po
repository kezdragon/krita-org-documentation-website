# Translation of docs_krita_org_reference_manual___main_menu___image_menu.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___image_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:47+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Separate Image"
msgstr "Розділити зображення"

#: ../../reference_manual/main_menu/image_menu.rst:1
msgid "The image menu in Krita."
msgstr "Меню «Зображення» у Krita."

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Image"
msgstr "Зображення"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Canvas Projection Color"
msgstr "Колір проєкції на полотні"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Trim"
msgstr "Обрізання"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Resize"
msgstr "Змінити розмір"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Scale"
msgstr "Масштаб"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Mirror"
msgstr "Віддзеркалення"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Transform"
msgstr "Перетворення"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Convert Color Space"
msgstr "Перетворити простір кольорів"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Offset"
msgstr "Зсув"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Split Channels"
msgstr "Розділити канали"

#: ../../reference_manual/main_menu/image_menu.rst:16
msgid "Image Menu"
msgstr "Меню «Зображення»"

#: ../../reference_manual/main_menu/image_menu.rst:18
msgid "Properties"
msgstr "Властивості"

#: ../../reference_manual/main_menu/image_menu.rst:19
msgid "Gives you the image properties."
msgstr "Відкриває вікно властивостей зображення."

#: ../../reference_manual/main_menu/image_menu.rst:20
msgid "Image Background Color and Transparency"
msgstr "Колір та прозорість тла зображення"

#: ../../reference_manual/main_menu/image_menu.rst:21
msgid "Change the background canvas color."
msgstr "Надає змогу змінити колір тла полотна."

#: ../../reference_manual/main_menu/image_menu.rst:22
msgid "Convert Current Image Color Space."
msgstr "Перетворити простір кольорів зображення"

#: ../../reference_manual/main_menu/image_menu.rst:23
msgid "Converts the current image to a new colorspace."
msgstr "Змінити поточний простір кольорів зображення на новий."

#: ../../reference_manual/main_menu/image_menu.rst:24
msgid "Trim to image size"
msgstr "Обрізати за розмірами зображення"

#: ../../reference_manual/main_menu/image_menu.rst:25
msgid ""
"Trims all layers to the image size. Useful for reducing filesize at the loss "
"of information."
msgstr ""
"Обрізати усі шари за розміром зображення. Корисно для зменшення розміру "
"файла, але із втратою частини даних."

#: ../../reference_manual/main_menu/image_menu.rst:26
msgid "Trim to Current Layer"
msgstr "Обрізати до поточного шару"

#: ../../reference_manual/main_menu/image_menu.rst:27
msgid ""
"A lazy cropping function. Krita will use the size of the current layer to "
"determine where to crop."
msgstr ""
"Функція «лінивого» обрізання. Krita використає розміри поточного шару для "
"визначення меж області обрізання."

#: ../../reference_manual/main_menu/image_menu.rst:28
msgid "Trim to Selection"
msgstr "Обрізати до позначеного"

#: ../../reference_manual/main_menu/image_menu.rst:29
msgid ""
"A lazy cropping function. Krita will crop the canvas to the selected area."
msgstr ""
"Функція «лінивого» обрізання. Krita обріже полотно до позначеної області."

#: ../../reference_manual/main_menu/image_menu.rst:30
msgid "Rotate Image"
msgstr "Обернути зображення"

#: ../../reference_manual/main_menu/image_menu.rst:31
msgid "Rotate the image"
msgstr "Обертати зображення."

#: ../../reference_manual/main_menu/image_menu.rst:32
msgid "Shear Image"
msgstr "Перекосити зображення"

#: ../../reference_manual/main_menu/image_menu.rst:33
msgid "Shear the image"
msgstr "Перекосити зображення."

#: ../../reference_manual/main_menu/image_menu.rst:34
msgid "Mirror Image Horizontally"
msgstr "Віддзеркалити зображення горизонтально"

#: ../../reference_manual/main_menu/image_menu.rst:35
msgid "Mirror the image on the horizontal axis."
msgstr "Віддзеркалити зображення горизонтально."

#: ../../reference_manual/main_menu/image_menu.rst:36
msgid "Mirror Image Vertically"
msgstr "Віддзеркалити зображення вертикально"

#: ../../reference_manual/main_menu/image_menu.rst:37
msgid "Mirror the image on the vertical axis."
msgstr "Віддзеркалити зображення вертикально."

#: ../../reference_manual/main_menu/image_menu.rst:38
msgid "Scale to New Size"
msgstr "Змінити розмір на інший"

#: ../../reference_manual/main_menu/image_menu.rst:39
msgid ""
"The resize function in any other program with the :kbd:`Ctrl + Alt + I` "
"shortcut."
msgstr ""
"Функціональна можливість зміни розміру у будь-якій іншій програмі, :kbd:"
"`Ctrl + Alt + I`."

#: ../../reference_manual/main_menu/image_menu.rst:40
msgid "Offset Image"
msgstr "Змістити зображення"

#: ../../reference_manual/main_menu/image_menu.rst:41
msgid "Offset all layers."
msgstr "Зсунути усі шари."

#: ../../reference_manual/main_menu/image_menu.rst:42
msgid "Resize Canvas"
msgstr "Змінити розміри полотна"

#: ../../reference_manual/main_menu/image_menu.rst:43
msgid "Change the canvas size. Don't confuse this with Scale to new size."
msgstr ""
"Змінити розмір полотна. Не плутайте цю дію із дією :guilabel:`Змінити розмір "
"на інший`."

#: ../../reference_manual/main_menu/image_menu.rst:44
msgid "Image Split"
msgstr "Поділ зображень"

#: ../../reference_manual/main_menu/image_menu.rst:45
msgid "Calls up the :ref:`image_split` dialog."
msgstr "Викликає діалогове вікно :ref:`image_split`."

#: ../../reference_manual/main_menu/image_menu.rst:46
msgid "Wavelet Decompose"
msgstr "Розклад на вейвлети"

#: ../../reference_manual/main_menu/image_menu.rst:47
msgid "Does :ref:`wavelet_decompose` on the current layer."
msgstr "Виконує дію :ref:`wavelet_decompose` над поточним шаром."

#: ../../reference_manual/main_menu/image_menu.rst:49
msgid ":ref:`Separates <separate_image>` the image into channels."
msgstr ":ref:`Розділити <separate_image>` зображення на канали."

#~ msgid "Splits the image."
#~ msgstr "Розділити зображення."
