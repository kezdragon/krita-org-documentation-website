# Translation of docs_krita_org_reference_manual___preferences___color_management_settings.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___color_management_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:37+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/preferences/color_management_settings.rst:1
msgid "The color management settings in Krita."
msgstr "Параметри керування кольорами у Krita."

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Preferences"
msgstr "Налаштування"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Color Management"
msgstr "Керування кольорами"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Softproofing"
msgstr "Проба кольорів"

#: ../../reference_manual/preferences/color_management_settings.rst:17
msgid "Color Management Settings"
msgstr "Параметри керування кольорами"

#: ../../reference_manual/preferences/color_management_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Color_Management.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Color_Management.png"

#: ../../reference_manual/preferences/color_management_settings.rst:21
msgid ""
"Krita offers extensive functionality for color management, utilising `Little "
"CMS <http://www.littlecms.com/>`_ We describe Color Management in a more "
"overall level here: :ref:`color_managed_workflow`."
msgstr ""
"У Krita передбачено широкі можливості з керування кольорами. Для цього ми "
"використали `Little CMS <http://www.littlecms.com/>`_ Докладний опис "
"керування кольорами можна знайти у розділі :ref:`color_managed_workflow`."

#: ../../reference_manual/preferences/color_management_settings.rst:25
msgid "General"
msgstr "Загальне"

#: ../../reference_manual/preferences/color_management_settings.rst:28
msgid "Default Color Model For New Images"
msgstr "Типова модель кольору для нових зображень:"

#: ../../reference_manual/preferences/color_management_settings.rst:30
msgid "Choose the default model you prefer for all your images."
msgstr ""
"Вибір типової моделі, яку слід використовувати для усіх ваших зображень."

#: ../../reference_manual/preferences/color_management_settings.rst:33
msgid "When Pasting Into Krita From Other Applications"
msgstr "Коли вставляєте в Krita з інших програм"

#: ../../reference_manual/preferences/color_management_settings.rst:35
msgid ""
"The user can define what kind of conversion, if any, Krita will do to an "
"image that is copied from other applications i.e. Browser, GIMP, etc."
msgstr ""
"Користувач може визначити, якого типу перетворення буде виконано Krita із "
"зображенням, яке було скопійовано з іншої програми — браузера, GIMP тощо."

#: ../../reference_manual/preferences/color_management_settings.rst:37
msgid "Assume sRGB"
msgstr "Припускати sRGB"

#: ../../reference_manual/preferences/color_management_settings.rst:38
msgid ""
"This option will show the pasted image in the default Krita ICC profile of "
"sRGB."
msgstr ""
"За допомогою цього пункту можна визначити, що вставлене зображення буде "
"показано із використанням типового профілю ICC Krita — sRGB."

#: ../../reference_manual/preferences/color_management_settings.rst:39
msgid "Assume monitor profile"
msgstr "Припускати профіль монітора"

#: ../../reference_manual/preferences/color_management_settings.rst:40
msgid ""
"This option will show the pasted image in the monitor profile selected in "
"system preferences."
msgstr ""
"Цей пункт покаже вставлене зображення у профілі монітора, який вибрано у "
"налаштуваннях системи."

#: ../../reference_manual/preferences/color_management_settings.rst:42
msgid "Ask each time"
msgstr "Запитувати щоразу"

#: ../../reference_manual/preferences/color_management_settings.rst:42
msgid ""
"Krita will ask the user each time an image is pasted, what to do with it. "
"This is the default."
msgstr ""
"Krita питатиме користувача про те, що слід зробити, під час кожного "
"вставлення зображення. Це типова поведінка програми."

#: ../../reference_manual/preferences/color_management_settings.rst:46
msgid ""
"When copying and pasting in Krita color information is always preserved."
msgstr ""
"При копіюванні і вставленні у Krita дані щодо кольорів завжди зберігаються."

#: ../../reference_manual/preferences/color_management_settings.rst:49
msgid "Use Blackpoint Compensation"
msgstr "Використовувати компенсацію точки чорного"

#: ../../reference_manual/preferences/color_management_settings.rst:51
msgid ""
"This option will turn on Blackpoint Compensation for the conversion. BPC is "
"explained by the maintainer of LCMS as following:"
msgstr ""
"За допомогою цього пункту можна увімкнути компенсацію точки чорного кольору "
"для перетворення. Ось пояснення щодо компенсації точки чорного кольору від "
"супровідника LCMS:"

#: ../../reference_manual/preferences/color_management_settings.rst:53
msgid ""
"BPC is a sort of \"poor man's\" gamut mapping. It basically adjust contrast "
"of images in a way that darkest tone of source device gets mapped to darkest "
"tone of destination device. If you have an image that is adjusted to be "
"displayed on a monitor, and want to print it on a large format printer, you "
"should realize printer can render black significantly darker that the "
"screen. So BPC can do the adjustment for you. It only makes sense on "
"Relative colorimetric intent. Perceptual and Saturation does have an "
"implicit BPC."
msgstr ""
"Компенсація точки чорного є певним чином прив'язкою гами «для бідних». На "
"базовому рівні, вона коригує контрастність зображень так, що найтемніший тон "
"на початковому пристрої прив'язується до найтемнішого тону на пристрої "
"призначення. Якщо у вас є зображення, яке скориговано для належного показу "
"на моніторі, і ви хочете надрукувати його на великоформатному принтері, вам "
"слід взяти до уваги те, що принтер може відтворювати чорний колір, який є "
"значно темнішим за чорний колір на екрані. Компенсація точки чорного може "
"виконати відповідне коригування кольорів. Цю компенсацію має сенс "
"застосовувати лише при відносному колориметричному відтворенні кольорів. При "
"придатному до сприйняття відтворенні та відтворенні за насиченістю "
"компенсація точки чорного виконується неявним чином."

#: ../../reference_manual/preferences/color_management_settings.rst:56
msgid "Allow LittleCMS optimizations"
msgstr "Уможливити оптимізації LittleCMS"

#: ../../reference_manual/preferences/color_management_settings.rst:58
msgid "Uncheck this option when using Linear Light RGB or XYZ."
msgstr ""
"Зніміть позначку з цього пункту, якщо використовуєте лінійний простий RGB "
"або XYZ."

#: ../../reference_manual/preferences/color_management_settings.rst:61
msgid "Display"
msgstr "Показ"

#: ../../reference_manual/preferences/color_management_settings.rst:63
msgid "Use System Monitor Profile"
msgstr "Використовувати профіль монітора системи"

#: ../../reference_manual/preferences/color_management_settings.rst:64
msgid ""
"This option when selected will tell Krita to use the ICC profile selected in "
"your system preferences."
msgstr ""
"Якщо позначено цей пункт, Krita використовуватиме профіль ICC, який вибрано "
"у налаштуваннях вашої системи."

#: ../../reference_manual/preferences/color_management_settings.rst:65
msgid "Screen Profiles"
msgstr "Профілі екрана"

#: ../../reference_manual/preferences/color_management_settings.rst:66
msgid ""
"There are as many of these as you have screens connected. The user can "
"select an ICC profile which Krita will use independent of the monitor "
"profile set in system preferences. The default is sRGB built-in. On Unix "
"systems, profile stored in $/usr/share/color/icc (system location) or $~/."
"local/share/color/icc (local location) will be proposed. Profile stored in "
"Krita preference folder, $~/.local/share/krita/profiles will be visible only "
"in Krita."
msgstr ""
"Таких профілів має бути стільки, скільки з'єднано екранів з комп'ютером. "
"Користувач може вибрати профіль ICC, який Krita використовуватиме незалежно "
"від профілю монітора, який встановлено у налаштуваннях системи. Типовим є "
"вбудований профіль sRGB. У системах Unix буде запропоновано профіль, який "
"зберігається у $/usr/share/color/icc (загальносистемне місце) або у $~/."
"local/share/color/icc (локальне місце). Профіль, який зберігається у теці "
"налаштувань Krita, $~/.local/share/krita/profiles буде видимим лише у Krita."

#: ../../reference_manual/preferences/color_management_settings.rst:68
msgid "Rendering Intent"
msgstr "Відтворення кольорів"

#: ../../reference_manual/preferences/color_management_settings.rst:68
msgid ""
"Your choice of rendering intents is a way of telling Littlecms how you want "
"colors mapped from one color space to another. There are four options "
"available, all are explained on the :ref:`icc_profiles` manual page."
msgstr ""
"Ваш вибір способу відтворення кольорів визначає, як Littlecms має пов'язати "
"кольори двох різних просторів кольорів. Передбачено чотири варіанти, "
"докладний опис яких наведено у розділі :ref:`icc_profiles`."

#: ../../reference_manual/preferences/color_management_settings.rst:71
msgid "Softproofing options"
msgstr "Параметри проби кольорів"

#: ../../reference_manual/preferences/color_management_settings.rst:73
msgid ""
"These allow you to configure the *default* softproofing options. To "
"configure the actual softproofing for the current image, go to :"
"menuselection:`Image --> Image Properties --> Softproofing` ."
msgstr ""
"За допомогою цих параметрів ви можете налаштувати *типову* пробу кольорів. "
"Щоб налаштувати справжню пробу кольорів для поточного зображення, "
"скористайтеся пунктом меню :menuselection:`Зображення --> Властивості "
"зображення --> Проба кольорів`."

#: ../../reference_manual/preferences/color_management_settings.rst:75
msgid ""
"For indepth details about how to use softproofing, check out :ref:`the page "
"on softproofing <soft_proofing>`."
msgstr ""
"Щоб дізнатися більше про використання проби кольорів, ознайомтеся із :ref:"
"`розділом щодо проби кольорів <soft_proofing>`."
