# translation of docs_krita_org_general_concepts___file_formats___file_kpl.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_kpl\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-01 13:54+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../general_concepts/file_formats/file_kpl.rst:1
msgid "The Krita Palette file format."
msgstr "Súborový formát Krita Paleta."

#: ../../general_concepts/file_formats/file_kpl.rst:10
#, fuzzy
#| msgid "\\*.kpl"
msgid "*.kpl"
msgstr "\\*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "KPL"
msgstr ""

#: ../../general_concepts/file_formats/file_kpl.rst:10
#, fuzzy
#| msgid "The Krita Palette file format."
msgid "Krita Palette"
msgstr "Súborový formát Krita Paleta."

#: ../../general_concepts/file_formats/file_kpl.rst:15
msgid "\\*.kpl"
msgstr "\\*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:17
msgid ""
"Since 4.0, Krita has a new palette file-format that can handle colors that "
"are wide gamut, RGB, CMYK, XYZ, GRAY, or LAB, and can be of any of the "
"available bitdepths, as well as groups. These are Krita Palettes, or ``*."
"kpl``."
msgstr ""

#: ../../general_concepts/file_formats/file_kpl.rst:19
msgid ""
"``*.kpl`` files are ZIP files, with two XMLs and ICC profiles inside. The "
"colorset XML contains the swatches as ColorSetEntry and Groups as Group. The "
"profiles.XML contains a list of profiles, and the ICC profiles themselves "
"are embedded to ensure compatibility over different computers."
msgstr ""
