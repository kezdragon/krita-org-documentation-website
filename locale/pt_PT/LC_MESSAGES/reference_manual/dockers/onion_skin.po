# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 10:02+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Krita image Onionskin images Onionskindocker\n"
"X-POFile-SpellExtra: dockers guilabel\n"

#: ../../reference_manual/dockers/onion_skin.rst:1
msgid "Overview of the onion skin docker."
msgstr "Introdução à área da 'pele de cebola'."

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Animation"
msgstr "Animação"

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Onion Skin"
msgstr "Pele de Cebola"

#: ../../reference_manual/dockers/onion_skin.rst:15
msgid "Onion Skin Docker"
msgstr "Área da Pele de Cebola"

#: ../../reference_manual/dockers/onion_skin.rst:18
msgid ".. image:: images/dockers/Onion_skin_docker.png"
msgstr ".. image:: images/dockers/Onion_skin_docker.png"

#: ../../reference_manual/dockers/onion_skin.rst:19
msgid ""
"To make animation easier, it helps to see both the next frame as well as the "
"previous frame sort of layered on top of the current. This is called *onion-"
"skinning*."
msgstr ""
"Para tornar a animação mais simples, ajuda poder ver a imagem anterior e a "
"seguinte como que 'sobrepostas' à actual. Este processo chama-se *pele de "
"cebola*."

#: ../../reference_manual/dockers/onion_skin.rst:22
msgid ".. image:: images/dockers/Onion_skin_01.png"
msgstr ".. image:: images/dockers/Onion_skin_01.png"

#: ../../reference_manual/dockers/onion_skin.rst:23
msgid ""
"Basically, they are images that represent the frames before and after the "
"current frame, usually colored or tinted."
msgstr ""
"Basicamente, são imagens que representam as imagens antes e depois da "
"actual, estando normalmente coloridas ou pintadas."

#: ../../reference_manual/dockers/onion_skin.rst:25
msgid ""
"You can toggle them by clicking the lightbulb icon on a layer that is "
"animated (so, has frames), and isn’t fully opaque. (Krita will consider "
"white to be white, not transparent, so don’t animated on an opaque layer if "
"you want onion skins.)"
msgstr ""
"Podê-las-á activar/desactivar se carregar no ícone da lâmpada sobre uma "
"camada que esteja animada (que tenha imagens por isso) e que não seja "
"completamente opaca. (O Krita irá considerar o branco como branco, não "
"transparente, como tal não anime uma camada opaca se quiser 'peles de "
"cebola'.)"

#: ../../reference_manual/dockers/onion_skin.rst:29
msgid ""
"Since 4.2 onion skins are disabled on layers whose default pixel is fully "
"opaque. These layers can currently only be created by using :guilabel:"
"`background as raster layer` in the :guilabel:`content` section of the new "
"image dialog. Just don't try to animate on a layer like this if you rely on "
"onion skins, instead make a new one."
msgstr ""
"Desde o 4.2, as 'peles de cebola' estão desactivadas nas camadas cujos "
"pixels por omissão são completamente opacos. Estas camadas só podem ser "
"criadas de momento se usar a funcionalidade :guilabel:`fundo como camada "
"rasterizada` na secção de :guilabel:`conteúdo` da janela de nova imagem. "
"Simplesmente não tente animar sobre uma camada deste tipo se se basear em "
"'peles de cebola'; em vez disso, crie uma nova."

#: ../../reference_manual/dockers/onion_skin.rst:31
msgid ""
"The term onionskin comes from the fact that onions are semi-transparent. In "
"traditional animation animators would make their initial animations on "
"semitransparent paper on top of an light-table (of the special animators "
"variety), and they’d start with so called keyframes, and then draw frames in "
"between. For that, they would place said keyframes below the frame they were "
"working on, and the light table would make the lines of the keyframes shine "
"through, so they could reference them."
msgstr ""
"O termo 'pele de cebola' vem do facto que as cebolas são semi-transparentes. "
"Na animação tradicional, as animações eram feitas inicialmente em papel semi-"
"transparente sobre uma mesa de luz (ou o equivalente dos animadores "
"especiais), sendo que eles iriam começar com as ditas 'imagens-chave', "
"desenhando então imagens intermédias. Para isso, eles iriam colocar as ditas "
"imagens-chave debaixo da imagem que estavam a trabalhar, assim como a mesa "
"de luz iria criar as linhas das imagens-chave a brilhar à transparência, "
"para que então as usassem como referência."

#: ../../reference_manual/dockers/onion_skin.rst:33
msgid ""
"Onion-skinning is a digital implementation of such a workflow, and it’s very "
"useful when trying to animate."
msgstr ""
"A 'pele de cebola' é uma implementação digital de uma destas operações, "
"sendo bastante útil ao tentar criar animações."

#: ../../reference_manual/dockers/onion_skin.rst:36
msgid ".. image:: images/dockers/Onion_skin_02.png"
msgstr ".. image:: images/dockers/Onion_skin_02.png"

#: ../../reference_manual/dockers/onion_skin.rst:37
msgid ""
"The slider and the button with zero offset control the master opacity and "
"visibility of all the onion skins. The boxes at the top allow you to toggle "
"them on and off quickly, the main slider in the middle is a sort of ‘master "
"transparency’ while the sliders to the side allow you to control the "
"transparency per keyframe offset."
msgstr ""
"A barra e o botão com o controlo de deslocamento a zero controlam a "
"opacidade-mestra e a visibilidade de todas as peles de cebola. As opções de "
"marcação no topo permitem-lhe ligá-las e desligá-las rapidamente; a barra "
"deslizante do meio é uma espécie de 'transparência-mestra', enquanto as "
"barras do lado permitem-lhe controlar o deslocamento da transparência por "
"imagem-chave."

#: ../../reference_manual/dockers/onion_skin.rst:39
msgid ""
"Tint controls how strongly the frames are tinted, the first screen has 100%, "
"which creates a silhouette, while below you can still see a bit of the "
"original colors at 50%."
msgstr ""
"A pintura controla com que força são pintadas as imagens, sendo que o "
"primeiro ecrã tem 100%, que cria uma silhueta, enquanto que abaixo ainda "
"poderá ver um pouco das cores originais a 50%."

#: ../../reference_manual/dockers/onion_skin.rst:41
msgid ""
"The :guilabel:`Previous Frame` and :guilabel:`Next Frame` color labels "
"allows you set the colors."
msgstr ""
"A :guilabel:`Imagem anterior` e :guilabel:`Imagem Seguinte` permitem-lhe "
"definir as cores."
