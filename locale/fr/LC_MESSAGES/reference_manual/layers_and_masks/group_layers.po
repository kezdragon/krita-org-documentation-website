msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-27 02:53+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/layers_and_masks/group_layers.rst:1
msgid "How to use group layers in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Layers"
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Groups"
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Passthrough Mode"
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:17
msgid "Group Layers"
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:19
msgid ""
"While working in complex artwork you'll often find the need to group the "
"layers or some portions and elements of the artwork in one unit. Group "
"layers come in handy for this, they allow you to make a segregate the "
"layers, so you can hide these quickly, or so you can apply a mask to all the "
"layers inside this group as if they are one, you can also recursively "
"transform the content of the group... Just drag the mask so it moves to the "
"layer. They are quickly made with the :kbd:`Ctrl + G` shortcut."
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:21
msgid ""
"A thing to note is that the layers inside a group layer are considered "
"separately when the layer gets composited, the layers inside a group are "
"separately composited and then this image is taken in to account when "
"compositing the whole image, while on the contrary, the groups in Photoshop "
"have something called pass-through mode which makes the layer behave as if "
"they are not in a group and get composited along with other layers of the "
"stack. The recent versions of Krita have pass-through mode you can enable it "
"to get similar behavior"
msgstr ""
