msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 08:14+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:32
msgid ""
".. image:: images/icons/polyline_tool.svg\n"
"   :alt: toolpolyline"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:1
msgid "Krita's polyline tool reference."
msgstr ""

#: ../../reference_manual/tools/polyline.rst:10
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:10
msgid "Polyline"
msgstr "Polyligne"

#: ../../reference_manual/tools/polyline.rst:15
msgid "Polyline Tool"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:17
msgid "|toolpolyline|"
msgstr ""

#: ../../reference_manual/tools/polyline.rst:20
msgid ""
"Polylines are drawn like :ref:`polygon_tool`, with the difference that the "
"double-click indicating the end of the polyline does not connect the last "
"vertex to the first one."
msgstr ""
